<?php
include('Model/joinevent.php');
if(isset($_SESSION['userInfo'])){
  if(isset($_GET['event'])){
    $event = getEvent($bdd, $_GET['event']);
    setlocale(LC_TIME, 'french');
    if(isset($_POST['inscription'])){
      if(!isUserParticipatingEvent($bdd, $event, $_SESSION['userInfo'])){
        updateEventPlaces(PDO $bdd, array $event){
  $q = $bdd->prepare('UPDATE events SET nb_place = :nbplaces WHERE id = :id');
  $q->execute(array(
    'nbplaces' => $event['nb_place'] - 1,
    'id' => $event['id']
  ));
}
        addUserInEvent(PDO $bdd, array $event, array $user){
    $q = $bdd->prepare('INSERT INTO user_participates_events(id_participant, id_event) VALUES(:id_user, :id_event)');
    $q->execute(array(
      'id_user' => $user['id'],
      'id_event' => $event['id']
    ));
  }
        $event = getEvent($bdd, $_GET['event']);
        $message = 'Votre inscription à cet événement a été prise en compte.';
      }
      else{
        $message = 'Vous participez déjà à cet événement.';
      }
    }
    if(isset($_POST['annuler'])){
      deleteEvent($bdd, $_GET['event']);

    }
  }
  else{
    header('Location:../Controller/calendar.php');
  }
}
else{
  header('Location:../Controller/connexion.php');
}
include('../View/joinevent.php');
getCreateurFromEvent(PDO $bdd, array $event){
  $q = $bdd->prepare('SELECT login FROM Users u INNER JOIN events e ON e.organizer_id = u.id WHERE e.id = :id');
  $q->execute(array(
    'id' => $event['id']
  ));
  $user = $q->fetch(PDO::FETCH_ASSOC);
  return $user['login'];
}
