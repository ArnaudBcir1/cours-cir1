<?php
if(isset($_SESSION['info'])){
  $user = $_SESSION['info'];
  $events =getEventsFromUser(PDO $bdd, array $user){
  if($user['rank'] == 'ORGANIZER'){
    $q = $bdd->prepare('SELECT * FROM events WHERE organizer_id = :id');
    $q->execute(array('id' => $user['id']));
    $events = $q->fetchAll(PDO::FETCH_ASSOC);
  }
  else if($user['rank'] == 'CUSTOMER'){
    $q = $bdd->prepare('SELECT * FROM events');
    $q->execute();
    $events = $q->fetchAll(PDO::FETCH_ASSOC);
  }
  return $events;
}
}
else{
  header('Location: ../Controller/connexion.php');
}
include('../View/allEvent.php');
?>
