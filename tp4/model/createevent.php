<?php
session_start();
try {
   $bdd = new PDO('mysql:host=localhost;dbname=calendar;charset=utf8', 'root', '');
} catch (Exception $e) {
      exit('Erreur de connexion à la base de données.');
}
if(isset($_POST['name']) && isset($_POST['description']) && isset($_POST['startdate']) && isset($_POST['enddate']) && isset($_POST['nb_place'])&& isset($_POST['organizer_id'])){
  $_name= filter_input(INPUT_POST,'name',FILTER_SANITIZE_SPECIAL_CHARS);
  $_description=filter_input(INPUT_POST,'description',FILTER_SANITIZE_SPECIAL_CHARS);
  $_organizer_id=filter_input(INPUT_POST,'organizer_id',FILTER_SANITIZE_SPECIAL_CHARS);
  $_nb_place=filter_input(INPUT_POST,'nb_place',FILTER_SANITIZE_NUMBER_INT);
}
if(isset($_POST['name']) && isset($_POST['description']) && isset($_POST['startdate']) && isset($_POST['enddate']) && isset($_POST['nb_place'])){
$req=$bdd->prepare('INSERT INTO events(name,description,startdate,enddate,organizer_id,nb_place) VALUES(:name,:description,:startdate,:enddate,:organizer_id,:nb_place)');
$req->execute(array(
  'name'=>$_POST['name'],
  'description'=>$_POST['description'],
  'startdate'=> str_replace('T',' ',$_POST['startdate']),
  'enddate'=> str_replace('T',' ',$_POST['enddate']),
  'organizer_id'=>$_SESSION['info']['id'],
  'nb_place'=>$_POST['nb_place']
));
}
