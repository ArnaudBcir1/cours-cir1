<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <title>Tous les événements</title>
  </head>

  <body>
    <?php if(isset($_GET['date'])){
      $dateCurrentDay = $_GET['date'];
      foreach ($events as $key => $event) {
        $startDateEvent = DateTime::createFromFormat('Y-m-d H:i:s', $event['startdate']);
        $endDateEvent = DateTime::createFromFormat('Y-m-d H:i:s', $event['enddate']);
        if(strtotime($dateCurrentDay) >= strtotime($startDateEvent->format('Y-m-d')) && strtotime($dateCurrentDay) <= strtotime($endDateEvent->format('Y-m-d'))){
          if($user['rank'] == 'ORGANIZER'){
            echo $event['name'] . ' ' . $event['description'] . '<br>';
          }
          else if($user['rank'] == 'CUSTOMER'){
            echo $event['name'] . '<br>';
          }
        }
      }
    } ?>
  </body>
</html>
