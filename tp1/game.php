<?php
<<<<<<< HEAD
function Cellule($square_size, $Tab1){
    for($i = 0; $i < $square_size; $i++){
      for($j = 0; $j < $square_size; $j++){
        $randomNumber = rand(0, 1);
        if($randomNumber == 0){
          $Tab1[$i][$j] = 'o';
        }
        else{
          $Tab1[$i][$j] = ' . ';
        }
      }
    }
    return $Tab1;
}

function Game($square_size, $Tab1){
  for($i = 0; $i < $square_size; $i++){
    for($j = 0; $j < $square_size; $j++){
       echo $Tab1[$i][$j];
    }
    echo '<br>';
  }
}

function nbNeighbours($linePosition, $columnPosition, $Tab1){
  $numberOfNeighbours = 0;
  if(isset($Tab1[$linePosition - 1][$columnPosition])){
    if($Tab1[$linePosition - 1][$columnPosition] == 'o'){
      $numberOfNeighbours++;
    }
  }
  if(isset($Tab1[$linePosition - 1][$columnPosition - 1])){
    if($Tab1[$linePosition - 1][$columnPosition - 1] == 'o'){
      $numberOfNeighbours++;
    }
  }
  if(isset($Tab1[$linePosition + 1][$columnPosition + 1])){
    if($Tab1[$linePosition + 1][$columnPosition + 1] == 'o'){
      $numberOfNeighbours++;
    }
  }
  if(isset($Tab1[$linePosition - 1][$columnPosition + 1])){
    if($Tab1[$linePosition - 1][$columnPosition + 1] == 'o'){
      $numberOfNeighbours++;
    }
  }
  if(isset($Tab1[$linePosition][$columnPosition - 1])){
    if($Tab1[$linePosition][$columnPosition - 1] == 'o'){
      $numberOfNeighbours++;
    }
  }
  if(isset($Tab1[$linePosition][$columnPosition + 1])){
    if($Tab1[$linePosition][$columnPosition + 1] == 'o'){
      $numberOfNeighbours++;
    }
  }

  return $numberOfNeighbours;
}

function newGen($square_size, $Tab1){
  $Tab2 = array();
  for($i = 0; $i < $square_size; $i++){
    $Tab2[$i] = array();
  }
  for($i = 0; $i < $square_size; $i++){
    for($j = 0; $j < $square_size; $j++){
      if($Tab1[$i][$j] == 'o'){
        if(nbNeighbours($i, $j, $Tab1) == 2 || nbNeighbours($i, $j, $Tab1) == 3){
          $Tab2[$i][$j] = 'o';
        }
        else{
          $Tab2[$i][$j] = ' . ';
        }
      }
      else{
        if(nbNeighbours($i, $j, $Tab1) == 3){
          $Tab2[$i][$j] = 'o';
        }
        else{
          $Tab2[$i][$j] = ' . ';
        }
      }
    }
  }
  return $Tab2;
=======
function initializeGame($nbOfRow, $nbOfCol) {
	$game = [];
	for($i = 0; $i < $nbOfRow; $i++) {
		$game[] = [];
		for($j = 0; $j < $nbOfCol; $j++) {
			$game[$i][$j] = (bool) random_int(0, 1);
		}
	}
	return $game;
}


function countNeightboors($game, $row, $col) {
	$neigboorsCoordinates = [[$row - 1, $col - 1], [$row - 1, $col], [$row - 1, $col + 1],
	                         [$row, $col - 1], [$row, $col + 1],
	                         [$row + 1, $col - 1], [$row + 1, $col], [$row + 1, $col + 1]];
	$neighboors = 0;
	foreach($neigboorsCoordinates as $coordinates) {
		if (isset($game[$coordinates[0]]) && isset($game[$coordinates[0]][$coordinates[1]])) {
			$neighboors += $game[$coordinates[0]][$coordinates[1]];
		}
	}
	return $neighboors;
}


function birth($array, $row, $col) {
	return (int) (countNeightboors($array, $row, $col) == 3);
}

function death($array, $row, $col) {
	return (int) !(countNeightboors($array, $row, $col) >= 4 || countNeightboors($array, $row, $col) <= 1);
}

function nextGeneration($oldGeneration) {
	$newGeneration = initializeGame(count($oldGeneration), count($oldGeneration[0]));
	foreach($oldGeneration as $rowIndex => $row) {
		foreach($row as $colIndex => $value) {
			$newGeneration[$rowIndex][$colIndex] = $value ? death($oldGeneration, $rowIndex, $colIndex) : birth($oldGeneration, $rowIndex, $colIndex);
		}
	}
	return $newGeneration;
}

function gameIsStill($old, $new) {
	foreach($old as $rowIndex => $row) {
		if (is_array($row) && !gameIsStill($row, $new[$rowIndex])) {
			return false;
		} else if (!is_array($row) && $row != $new[$rowIndex]) {
			return false;
		}
	}
	return true;
>>>>>>> cd02f0c74c48f9bd3e16f1d0b35e660ca2ae32f9
}
